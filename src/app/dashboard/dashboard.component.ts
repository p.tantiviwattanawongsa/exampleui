import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { ShareService } from '../ShareService';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {

  demoExam1Id: any;
  Lists: any;

  constructor(public dialog: MatDialog, private http: HttpClient, private sharedService: ShareService) { }
  ngOnInit(): void {
    this.getList();
  }

  getList() {
    this.http.get(this.sharedService.serverPath + '/ExamData/GetListDataExample').subscribe((res: any) => {
      this.Lists = res;
      console.log('this.Lists', this.Lists);
    });
  }

  loadInfo(item: any) {
    this.demoExam1Id = item.demoExam1Id;
  }
}
